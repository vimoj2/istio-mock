const fs = require('fs');
const jwt = require('jsonwebtoken');

const privateKey = fs.readFileSync('private.pem');

const issuer = 'jwt-issuer';
const subject = 'jwt-subject';
const audience = 'http://jwt.in.use';
const expiresIn = '1m';
const algorithm = 'RS256';

const payload = {
    data: 'data'
}
const signOptions = {
    issuer,
    subject,
    audience,
    expiresIn,
    algorithm,
};

const token = jwt.sign(payload, privateKey, signOptions);
console.log(token)